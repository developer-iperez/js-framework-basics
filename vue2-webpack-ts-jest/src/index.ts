import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './app/store/store'
import App from './app/App.vue';
import Main from './app/Main.vue'
import View1 from './app/View1.vue'
import View2 from './app/View2.vue'

Vue.config.productionTip = false
Vue.use(VueRouter)

// Configurar las rutas
const routes = [
    { path: '/', component: Main },
    { path: '/view1', component: View1 },
    { path: '/view2', component: View2 }
]

// Crear el enrutador
const router = new VueRouter({
  routes
})

new Vue({
  router,
  store,
  components: {
    App,
    Main,
    View1,
    View2
  },
  render: (h) => h(App)
}).$mount('#app')