export default function(value: number): string {
    if (!value)
        return ''

    const isoDateParts = new Date(value).toLocaleString().replace(',', '')
    return isoDateParts
}