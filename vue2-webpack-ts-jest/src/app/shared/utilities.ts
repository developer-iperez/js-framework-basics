export function getRandomNumber(min: number, max: number): number {
    return (Math.random() * (max - min + 1)) + min;
}

export function trunkDecimals(value: number, decimals: number) {
    const factor = Math.pow(10, decimals)
    return Math.floor(value * factor) / factor
}