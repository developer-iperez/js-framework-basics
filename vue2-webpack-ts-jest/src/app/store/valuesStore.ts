import { Commit } from 'vuex'
import { getRandomNumber, trunkDecimals } from '../shared/utilities'

interface RootState {
    values: Record<string, number>
    history: number[][]
}

function updateValues(commit: Commit): void {
    const value1 = Date.now()
    const value2 = Math.floor(getRandomNumber(10000, 60000))
    const value3 = value2 - 1000
    const value4 = trunkDecimals(getRandomNumber(-10, 10), 2)
    const updatedValues: Record<string, number> = {
        value1,
        value2,
        value3,
        value4
    };

    commit('updateValues', updatedValues);
    commit('addToHistory', [value1, value2, value3, value4]);
}

const state = {
    values: {
        value1: 0,
        value2: 0,
        value3: 0,
        value4: 0
    },
    history: []
}

const mutations = {
    updateValues(state: RootState, payload: Record<string, number>) {
        console.log('[ValuesStore] updateValues')

        state.values = payload
    },
    addToHistory(state: RootState, payload: number[]) {
        console.log('[ValuesStore] addToHistory')

        state.history.push(payload);
        if (state.history.length > 4) {
            state.history.shift()
        }
    }
}

const actions = {
    startUpdatingValues({ commit }: { commit: Commit }) {
        console.log('[ValuesStore] startUpdatingValues')

        updateValues(commit)
        setInterval(() => {
            updateValues(commit)
        }, 5000);
    }
}

const getters = {
    getValues: (state: RootState) => state.values,
    getHistory: (state: RootState) => state.history
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}