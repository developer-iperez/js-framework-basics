import Vue from 'vue'
import Vuex from 'vuex'
import values from './valuesStore'

Vue.use(Vuex)

/**
 * Links:
 * https://vuejs-tips.github.io/vuex-cheatsheet/
 * https://blog.logrocket.com/managing-multiple-store-modules-vuex/
 * https://vuex.vuejs.org/guide/modules.html
 */
export default new Vuex.Store({
    modules: {
        values
    }
})
