
Pasos para la preparación del proyecto
--------------------------------------
npm i vue@2.7.16
npm i vue-router@3.6.5
npm i vuex@3.6.2
npm i --save-dev webpack webpack-cli webpack-dev-server html-webpack-plugin copy-webpack-plugin
npm i --save-dev vue-loader@15.11.1
npm i --save-dev vue-template-compiler ts-loader typescript babel-loader @babel/core @babel/preset-env
npm i --save-dev sass sass-loader style-loader css-loader vue-style-loader
npm i --save-dev @types/webpack-env
npm i --save-dev jest

Nota: vue-loader 16+ isn't compatible with vue 2.x

Vue tips
--------

- Basic structure:
	- data
	x- computed
	x- methods

- Conditionals:
	- v-if
	- v-else-if
	- v-else
	- v-show
	
- Bucles:
	x- v-for
	
- Render:
	- v-html
	- :style / v-bind:style
	x- :class / v-bind:class
	
- Events:
	- @click / v-on:click
	- v-on:keyup
	- Mouse modifiers:
		.stop
		.prevent
		.capture
		.self
		.once
		.passive
	- Mouse modifiers:
		.left
		.right
		.middle	
	- Keyboard modifiers:
		.enter
		.tab
		.delete (captures both “Delete” and “Backspace” keys)
		.esc
		.space
		.up
		.down
		.left
		.right
	- Keyboard modifiers (system):
		.ctrl
		.alt
		.shift
		.meta
	
- Binding:
	- v-model
	
- Watchers:
	- watch
	
- Filters (only v2)
	x- filters
	
- Accedss to DOM elements:
	- ref
	
- Life-Cycle Hooks:
	- beforeCreate
	- creted
	- beforeMount
	- mounted
	- beforeUpdate
	- updated
	- beforeDestroy
	- destroy
	
- Custom Directivas:
	- Vue.directive
	- Hooks:
		- bind
		- inserted
		- updated
		- componentUpdated
		- unbind
	
- Componentes:
	x- components
	
- Routes:
	x- vue-router
	x- router-view

- Vuex
	x- vuex