const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')
// const devMode = process.env.NODE_ENV !== "production";

module.exports = {
    mode: 'development',
    entry: './src/index.ts', // Archivo principal de tu aplicación TypeScript
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['.ts', '.js', '.vue'], // Extensiones que webpack resolverá
        alias: {
            '@': path.resolve(__dirname, "./src/"),
            'vue$': 'vue/dist/vue.esm.js' // Alias para importar Vue.js
        }
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                use: {
                    loader: 'ts-loader',
                    options: {
                        appendTsSuffixTo: [/\.vue$/]
                    }
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'vue-style-loader', 'css-loader']
            },
            {
                test: /\.s[ac]ss$/i,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            template: 'public/index.html', // Ruta al archivo index.html
            filename: 'index.html' // Nombre del archivo en la carpeta dist
        }),
        new CopyWebpackPlugin({
            patterns: [
                { from: 'public/assets' }
            ]
        })
    ],
    devServer: {
        static: path.join(__dirname, 'dist'),
        compress: true,
        port: 5000 // Puerto del servidor de desarrollo
    }
};